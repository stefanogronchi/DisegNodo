#include "mainwindow.h"
#include "transitmodel.h"
#include "transitsortfilterproxymodel.h"
#include <QApplication>
#include <QtDebug>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    QApplication a(argc, argv);

    TransitModel model;
    model << Transit("Cadorna", "Novara", 07)
          << Transit("Cadorna", "Varese", 59)
          << Transit("Cadorna", "Como",   04)
          << Transit("Cadorna", "Laveno", 11)
          << Transit("Novara", "Cadorna", 53)
          << Transit("Varese", "Cadorna", 01)
          << Transit("Como",   "Cadorna", 57)
          << Transit("Laveno", "Cadorna", 59)
        ;
    TransitSortFilterProxyModel pm;
    pm.setSourceModel(&model);
    MainWindow w;
    w.setModel(&pm);
    w.show();
    return a.exec();
}
