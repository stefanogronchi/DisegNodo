#include "transitmodel.h"
#include <QSize>
#include <utility>
#include <algorithm>
#include <functional>
#include <iterator>

TransitModel::TransitModel(QObject *parent): QAbstractTableModel(parent)
{ }

int TransitModel::rowCount(const QModelIndex & /*parent*/) const Q_DECL_NOEXCEPT
{
    return m_data.size();
}

int TransitModel::columnCount(const QModelIndex & /*parent*/) const Q_DECL_NOTHROW
{
    return 4;
}

static bool isIndexValid(const QModelIndex &index, int size)
{
    return index.isValid()
        && index.row() < size
        && index.row() >= 0;
}

QVariant TransitModel::data(const QModelIndex &index, int role) const Q_DECL_NOEXCEPT
{
    QVariant res;

    if (role == Qt::TextAlignmentRole && index.column() == 0)
        res = (int)(Qt::AlignRight | Qt::AlignVCenter);

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        if (isIndexValid(index, m_data.size()))
        {
            const auto& tmp = m_data.at(row);
            switch (col) {
                case 0:
                    if (tmp.minute() >= 0)
                        res = tmp.minute();
                    break;
                case 1:
                    res = tmp.from();
                    break;
                case 2:
                    res = tmp.to();
                    break;
                case 3:
                    res = m_styles.at(row);
                    break;
                default:
                    break;
            }
        }
    }
    return res;
}

QVariant TransitModel::headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_NOEXCEPT
{
    QVariant res;
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch (section) {
                case 0:
                    res = tr("Minute");
                    break;
                case 1:
                    res = tr("From");
                    break;
                case 2:
                    res = tr("To");
                    break;
                case 3:
                    res = tr("Style");
                    break;
                default:
                    break;
            }
        }
        else {
            res = QString::number(section);
        }
    }
    return res;
}

Qt::ItemFlags TransitModel::flags(const QModelIndex &index) const Q_DECL_NOEXCEPT
{
    //static Qt::ItemFlags flags{Qt::ItemIsSelectable, Qt::ItemIsEditable, Qt::ItemIsEnabled, Qt::ItemNeverHasChildren};
    //return flags;
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

TransitModel &TransitModel::operator<<(Transit rhs)
{
    beginInsertRows(QModelIndex(),m_data.size(),m_data.size());
    try { m_data << std::move(rhs); m_styles << QString{}; }
    catch (...) { }
    endInsertRows();
    return *this;
}

bool TransitModel::insertRows(int position, int rows, const QModelIndex & /*parent*/) Q_DECL_NOEXCEPT
{
    if (position < 0 || position > m_data.size() || rows < 0)
        return false;

    bool res = false;
    beginInsertRows(QModelIndex(), position, position+rows-1);

    try {
        m_data.insert(position, rows, Transit{});
        m_styles.insert(position, rows, QString{});
        res = true;
    }
    catch (...) {}

    endInsertRows();
    return res;
}

bool TransitModel::removeRows(int position, int rows, const QModelIndex & /*parent*/) Q_DECL_NOEXCEPT
{
    if (position < 0 || position >= m_data.size() || rows < 0)
        return false;

    bool res = false;
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    try {
        auto bd = std::next(m_data.begin(), position);
        auto ed = std::next(bd, rows);
        m_data.erase(bd, ed);
        auto bs = std::next(m_styles.begin(), position);
        auto es = std::next(bs, rows);
        m_styles.erase(bs, es);
        res = true;
    }
    catch (...) {}

    endRemoveRows();
    return res;
}

bool TransitModel::setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_NOEXCEPT
{
    bool res = false;
    if (role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        try {
            if (isIndexValid(index, m_data.size()))
            {
                auto& tmp = m_data[row];
                switch (col) {
                    case 0:
                        tmp.setMinute(value.toInt());
                        break;
                    case 1:
                        tmp.setFrom(value.toString());
                        break;
                    case 2:
                        tmp.setTo(value.toString());
                        break;
                    case 3:
                        m_styles[row] = value.toString();
                        break;
                    default:
                        throw col;
                }
                res = true;
            }
        } catch (...) { }
    }
    if (res)
        emit dataChanged(index, index);
    return res;
}
