#include "transitstyle.h"

TransitStyle::TransitStyle()
{

}

QFont TransitStyle::font() const
{
    return m_font;
}

void TransitStyle::setFont(const QFont &font)
{
    m_font = font;
}

QBrush TransitStyle::foregroundBrush() const
{
    return m_fgBrush;
}

void TransitStyle::setForegroundBrush(const QBrush &fgBrush)
{
    m_fgBrush = fgBrush;
}

QPen TransitStyle::pen() const
{
    return m_pen;
}

void TransitStyle::setPen(const QPen &pen)
{
    m_pen = pen;
}

QBrush TransitStyle::backgroundBrush() const
{
    return m_bgBrush;
}

void TransitStyle::setBackgroundBrush(const QBrush &bgBrush)
{
    m_bgBrush = bgBrush;
}
