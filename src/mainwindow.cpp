#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "comboboxitemdelegate.h"
#include <QDebug>
#include <QSize>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->dataView->addAction(ui->action_AddRow);
    ui->dataView->addAction(ui->action_RemoveRow);
//    ui->dataView->setItemDelegateForColumn(3, new ComboBoxItemDelegate(, ));
    connect(ui->action_AddRow, &QAction::triggered, this, &MainWindow::addRow);
    connect(ui->action_RemoveRow, &QAction::triggered, this, &MainWindow::deleteSelectedRows);
}

QAbstractItemModel* MainWindow::model() const
{
    return ui->dataView->model();
}

void MainWindow::setModel(QAbstractItemModel *model)
{
    ui->dataView->setModel(model);
    //ui->graphicsView->setModel(model);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addRow()
{
    ui->dataView->model()->insertRow(ui->dataView->model()->rowCount());
}

void MainWindow::deleteSelectedRows()
{
    QModelIndexList rows = ui->dataView->selectionModel()->selectedRows();
    for(int i=0; i< rows.count(); i++) {
        ui->dataView->model()->removeRow(rows.at(i).row());
    }
}
