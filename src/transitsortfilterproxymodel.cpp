#include "transitsortfilterproxymodel.h"

TransitSortFilterProxyModel::TransitSortFilterProxyModel(QObject *parent): QSortFilterProxyModel(parent)
{ }

bool TransitSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    if (leftData.type() == QVariant::Int)
       return leftData.toInt() < rightData.toInt();
    else
       return QString::localeAwareCompare(leftData.toString(), rightData.toString()) < 0;
}
