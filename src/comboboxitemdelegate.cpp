#include "comboboxitemdelegate.h"
#include <QComboBox>

ComboBoxItemDelegate::ComboBoxItemDelegate(QAbstractItemModel *valueModel, QObject *parent): QStyledItemDelegate(parent), m_model(valueModel)
{ }

QWidget *ComboBoxItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & /*option*/, const QModelIndex & /*index*/) const
{
    QComboBox* comboBox = new QComboBox(parent);
    comboBox->setModel(m_model);
    return comboBox;
}

void ComboBoxItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(QComboBox* comboBox = qobject_cast<QComboBox*>(editor))
    {
        model->setData(index, comboBox->currentText(), Qt::EditRole);
    }
    else
    {
        QStyledItemDelegate::setModelData(editor, model, index);
    }
}

void ComboBoxItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(QComboBox* comboBox = qobject_cast<QComboBox*>(editor))
    {
        QString value = index.model()->data(index, Qt::EditRole).toString();
        int idx = comboBox->findText(value);
        if (idx >= 0)
            comboBox->setCurrentIndex(idx);
    }
    else
    {
        QStyledItemDelegate::setEditorData(editor, index);
    }
}

void ComboBoxItemDelegate::updateEditorGeometry(QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & /* index */) const
{
    editor->setGeometry(option.rect);
}

/*void ComboBoxItemDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    QStyleOptionViewItemV4 myOption = option;
    QString text = Items[index.row()].c_str();

    myOption.text = text;

    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
*/
