#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "transitmodel.h"
#include "transitview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);
    QAbstractItemModel* model() const;
    void setModel(QAbstractItemModel *model);

    ~MainWindow();

private slots:
    void addRow();
    void deleteSelectedRows();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
