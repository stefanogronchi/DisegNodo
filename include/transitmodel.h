#ifndef TRANSITMODEL_H
#define TRANSITMODEL_H

#include "transit.h"
#include <QAbstractTableModel>
#include <QVector>
#include <QString>

class TransitModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TransitModel(QObject *parent = Q_NULLPTR);

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_NOTHROW Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;

    TransitModel& operator<<(Transit rhs); // Transit is cheap to copy

    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex()) Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_NOEXCEPT Q_DECL_OVERRIDE;
private:
    QVector<Transit> m_data;
    QVector<QString> m_styles;
};

#endif // TRANSITMODEL_H
