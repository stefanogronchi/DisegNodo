#ifndef TRANSIT_H
#define TRANSIT_H

#include <QString>

class Transit
{
    QString m_from;
    QString m_to;
    int m_minute;

public:
    Transit();
    Transit(QString from, QString to, int minute);    

    // Accessors
    const QString& from() const;
    const QString& to() const;
    int minute() const;

    // Setters
    void setFrom(QString newFrom);
    void setTo(QString newTo);
    void setMinute(int newMinute);

    bool operator==(Transit rhs);
};

#endif // TRANSIT_H
