#-------------------------------------------------
#
# Project created by QtCreator 2017-03-29T14:40:39
#
#-------------------------------------------------

QT += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DisegNodo
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
           QT_NO_CAST_FROM_ASCII   # ensures all QString go through tr() or an explicit call

include(external/Qt-Color-Widgets/color_widgets.pri)

#top_builddir=$$shadowed($$PWD)

INCLUDEPATH += $$PWD/include

SOURCES += $$PWD/src/main.cpp\
           $$PWD/src/mainwindow.cpp \
           $$PWD/src/transit.cpp \
           $$PWD/src/transitmodel.cpp \
           $$PWD/src/transitview.cpp \
           $$PWD/src/comboboxitemdelegate.cpp \
           $$PWD/src/transitsortfilterproxymodel.cpp \
   # stylemodel.cpp \
           $$PWD/src/transitstyle.cpp

HEADERS += $$PWD/include/mainwindow.h \
           $$PWD/include/transit.h \
           $$PWD/include/transitmodel.h \
           $$PWD/include/transitview.h \
           $$PWD/include/comboboxitemdelegate.h \
           $$PWD/include/transitsortfilterproxymodel.h \
   # stylemodel.h \
           $$PWD/include/transitstyle.h

FORMS += $$PWD/forms/mainwindow.ui
